/**
 * @jest-environment jsdom
 */

import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom";
import App from "../components/App";
import axiosModule from "../libs/axios.min.js";

// Mock Axios
jest.mock("../libs/axios.min.js");
//import Modal from "react-modal";

jest.mock("react-modal");
Modal.setAppElement = jest.fn();

// Mock the API module
jest.mock("../components/api", () => ({
  getRestaurants: jest.fn(() => Promise.resolve({ data: [] })),
  addRestaurant: jest.fn(() => Promise.resolve({ data: {} })),
  updateRatings: jest.fn(() => Promise.resolve({ data: {} })),
  deleteRating: jest.fn(() => Promise.resolve({ data: {} })),
  addRating: jest.fn(() => Promise.resolve({ data: {} })),
}));

describe("App Component", () => {
  beforeEach(() => {
    // Clear mocks before each test
    jest.clearAllMocks();
  });

  afterEach(() => {
    jest.resetAllMocks();
    jest.resetModules();
  });

  test("Example test", () => {
    // Example test case
    expect(true).toBe(true);
  });

  test("renders without crashing", () => {
    render(<App />);
    // You can add more assertions based on your app structure
  });

  /*

  test('displays the "Add Restaurant" button', () => {
    const { getByText } = render(<App />);
    const addButton = getByText("Add Restaurant");
    expect(addButton).toBeInTheDocument();
  });

  test('opens the modal when "Add Restaurant" button is clicked', () => {
    const { getByText } = render(<App />);
    const addButton = getByText("Add Restaurant");
    fireEvent.click(addButton);

    // Ensure that the modal is rendered
    const modalTtestle = getByText("Restaurant Name");
    expect(modalTtestle).toBeInTheDocument();
  });

  test("submit the form in the modal", async () => {
    const { getByText, getByLabelText } = render(<App />);
    const addButton = getByText("Add Restaurant");
    fireEvent.click(addButton);

    // Fill in form fields
    fireEvent.change(getByLabelText("Restaurant Name"), {
      target: { value: "Test Restaurant" },
    });
    // Add more fireEvent.change for other form fields

    // Submtest the form
    fireEvent.click(getByText("Submit"));

    // Watest for the async operation to complete
    await waitFor(() => {
      // Add assertions based on the expected behavior after form submission
    });
  });*/

  // Add more test cases as needed
});
