// api.js

import axios from "axios";

const BASE_URL = "http://localhost:4000";

export const getRestaurants = () => {
  return axios.get(`${BASE_URL}/restaurants`);
};

export const addRestaurant = (formData) => {
  return axios.post(`${BASE_URL}/restaurants`, formData);
};

export const updateRatings = (id, ratings) => {
  return axios.put(`${BASE_URL}/api/ratings`, { id, ratings });
};

export const addRating = (id, rating, comment) => {
  return axios.post(`${BASE_URL}/api/ratings`, { id, rating, comment });
};

export const deleteRating = (restaurantId, ratingId) => {
  return axios.delete(`${BASE_URL}/api/ratings`, {
    data: { restaurantId, ratingId },
  });
};
