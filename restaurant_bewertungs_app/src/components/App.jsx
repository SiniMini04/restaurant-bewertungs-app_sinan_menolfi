import React, { useState, useEffect } from "react";
import * as api from "./api";
import StarRatings from "react-star-ratings";
import Modal from "react-modal";
import { useDropzone } from "react-dropzone";
import "./css/App.css";

Modal.setAppElement("#root");

function App() {
  const [restaurants, setRestaurants] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [rating, setRating] = useState(0);
  const [comment, setComment] = useState("");
  const [isRatingOpen, setIsRatingOpen] = useState(false);
  const [showTopFive, setShowTopFive] = useState(false);
  const [newRestaurant, setNewRestaurant] = useState({
    name: "",
    description: "",
    address: "",
    image: null,
    cheapestMenu: {
      name: "",
      price: 0,
    },
    expensiveWine: {
      name: "",
      price: 0,
    },
  });
  const [isPopupOpen, setIsPopupOpen] = useState(false);
  const [selectedRestaurant, setSelectedRestaurant] = useState(null);
  const [ratingIndex, setRatingIndex] = useState(null);
  const [newRating, setNewRating] = useState(0);
  const [newComment, setNewComment] = useState("");
  const [ratingClickTriggered, setRatingClickTriggered] = useState(false);
  const [updateRatingIndex, setUpdateRatingIndex] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");
  const [menuPrice, setMenuPrice] = useState("");
  const [isMenuPriceSearchActive, setIsMenuPriceSearchActive] = useState(false);
  const [winePrice, setWinePrice] = useState("");
  const [isWinePriceSearchActive, setIsWinePriceSearchActive] = useState(false);

  useEffect(() => {
    api.getRestaurants().then((response) => setRestaurants(response.data));
  }, []);

  useEffect(() => {
    return () => {
      setRatingClickTriggered(false);
    };
  }, [isPopupOpen]);

  useEffect(() => {
    if (updateRatingIndex && selectedRestaurant) {
      setRatingIndex(ratingIndex);
      setNewRating(selectedRestaurant.ratings[ratingIndex]?.rating || 0);
      setUpdateRatingIndex(false);
    }
  }, [updateRatingIndex, ratingIndex, selectedRestaurant]);

  const sortedRestaurants = [...restaurants].sort((a, b) => {
    const totalStarsA = a.ratings.reduce(
      (total, rating) => total + rating.rating,
      0
    );
    const totalStarsB = b.ratings.reduce(
      (total, rating) => total + rating.rating,
      0
    );
    const averageStarsA =
      a.ratings.length > 0 ? totalStarsA / a.ratings.length : 0;
    const averageStarsB =
      b.ratings.length > 0 ? totalStarsB / b.ratings.length : 0;

    return averageStarsB - averageStarsA;
  });

  const restaurantsToDisplay = isWinePriceSearchActive
    ? restaurants
        .filter(
          (restaurant) =>
            restaurant.expensiveWine &&
            restaurant.expensiveWine.price >= parseFloat(winePrice)
        )
        .slice(0, 5)
    : showTopFive
    ? sortedRestaurants.slice(0, 5)
    : restaurants
        .filter((restaurant) =>
          restaurant.name.toLowerCase().includes(searchTerm.toLowerCase())
        )
        .filter(
          (restaurant) =>
            !isMenuPriceSearchActive ||
            (restaurant.cheapestMenu &&
              restaurant.cheapestMenu.price <= parseFloat(menuPrice))
        )
        .slice(0, isMenuPriceSearchActive ? 4 : restaurants.length);

  const handleRatingClick = (index) => {
    if (!ratingClickTriggered) {
      setRatingClickTriggered(true);
      setRatingIndex(index);
      setUpdateRatingIndex(true);
    }
  };

  const handleAddRestaurant = () => {
    setIsModalOpen(true);
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    // Check if the input field is related to prices and validate numeric input
    if (
      (name === "cheapestMenuPrice" ||
        name === "expensiveWinePrice" ||
        name === "menuPrice" ||
        name === "winePrice") &&
      value !== "" &&
      !/^\d*\.?\d*$/.test(value)
    ) {
      // Display an error or handle it in another way (e.g., prevent submission)
      alert("Please enter a valid numeric value.");
      return;
    }

    // Update state for other input fields
    setNewRestaurant({
      ...newRestaurant,
      [name]: value,
    });
  };

  const onDrop = (acceptedFiles) => {
    const file = acceptedFiles[0];
    const reader = new FileReader();

    reader.onloadend = () => {
      setNewRestaurant({
        ...newRestaurant,
        image: reader.result, // This will store the base64 representation of the image
      });
    };

    reader.readAsDataURL(file);
  };

  const { getRootProps, getInputProps, open } = useDropzone({
    accept: {
      "image/png": [".png"],
      "image/jpeg": [".jpg", ".jpeg"],
    },
    noClick: true,
    noKeyboard: true,
    onDrop,
  });

  const handleSubmit = async (event) => {
    event.preventDefault();

    const formData = {
      name: newRestaurant.name,
      description: newRestaurant.description,
      address: newRestaurant.address,
      image: newRestaurant.image,
      cheapestMenu: {
        name: newRestaurant.cheapestMenuName,
        price: parseFloat(newRestaurant.cheapestMenuPrice),
      },
      expensiveWine: {
        name: newRestaurant.expensiveWineName,
        price: parseFloat(newRestaurant.expensiveWinePrice),
      },
    };

    if (doesRestaurantExist(newRestaurant)) {
      alert("A restaurant with this name and address already exists.");
      return;
    }

    try {
      const response = await api.addRestaurant(formData);
      window.location.reload();
      console.log(response.data);
    } catch (error) {
      console.error("Error:", error.response.status, error.response.statusText);
    }

    setIsModalOpen(false);
  };

  function doesRestaurantExist(newRestaurant) {
    return restaurants.some(
      (restaurant) =>
        restaurant.name === newRestaurant.name &&
        restaurant.address === newRestaurant.address
    );
  }

  function handleRestaurantClick(restaurant) {
    setSelectedRestaurant(restaurant);
    setIsPopupOpen(true);
  }

  const handleRatingSubmit = async (e, index) => {
    e.preventDefault();
    try {
      let updatedRatings = [...selectedRestaurant.ratings];

      if (index !== undefined) {
        // Update existing rating
        updatedRatings[index].rating = newRating;
        // Preserve the existing comment if the new comment is empty
        updatedRatings[index].comment =
          newComment || updatedRatings[index].comment;
      } else {
        // Add new rating
        const newRatingObj = {
          rating: newRating,
          comment: newComment,
        };
        updatedRatings.push(newRatingObj);
      }

      // Update rating in the backend using the api.js file
      const response = await api.updateRatings(
        selectedRestaurant._id,
        updatedRatings
      );

      if (response.status === 200) {
        // Update local state if the backend update is successful
        setSelectedRestaurant((prevState) => ({
          ...prevState,
          ratings: updatedRatings,
        }));
      } else {
        console.error(
          "Failed to update ratings:",
          response.status,
          response.statusText
        );
      }
    } catch (error) {
      console.error("Failed to update ratings:", error);
    }

    setRatingIndex(null);
    setRating(0);
    setComment("");
    setIsRatingOpen(false);
    setIsPopupOpen(false);
  };

  const onSubmitRating = async () => {
    try {
      if (selectedRestaurant) {
        const ratingData = {
          rating: rating,
          comment: comment,
        };

        const response = await api.addRating(
          selectedRestaurant._id,
          ratingData.rating,
          ratingData.comment
        );

        if (response.status === 200) {
          console.log("Rating submitted");
          // Fetch updated restaurant data after submitting the rating
          const updatedRestaurants = await api.getRestaurants();
          console.log("updatedRestaurants", updatedRestaurants);
          setRestaurants(updatedRestaurants.data);
        } else {
          console.error("Failed to submit rating");
        }
      }
    } catch (error) {
      console.error("Failed to submit rating", error);
    }

    setIsRatingOpen(false);
    setIsPopupOpen(false);
    console.log(isRatingOpen);
  };

  function getAverageRating() {
    if (selectedRestaurant.ratings && selectedRestaurant.ratings.length > 0) {
      const sum = selectedRestaurant.ratings.reduce((a, b) => a + b.rating, 0);
      return sum / selectedRestaurant.ratings.length;
    }
    return 0;
  }

  const handleDeleteComment = async (index) => {
    try {
      if (selectedRestaurant && selectedRestaurant.ratings[index]) {
        const ratingId = selectedRestaurant.ratings[index]._id;

        const response = await api.deleteRating(
          selectedRestaurant._id,
          ratingId
        );

        if (response.status !== 200) {
          throw new Error(`HTTP error! status: ${response.status}`);
        }

        // Remove the rating from the local state
        const newRatings = [...selectedRestaurant.ratings];
        newRatings.splice(index, 1);
        setSelectedRestaurant({
          ...selectedRestaurant,
          ratings: newRatings,
        });
      }
    } catch (error) {
      console.error("Failed to delete rating:", error);
    }
    setIsRatingOpen(false);
    setIsPopupOpen(false);
    window.location.reload();
  };

  function setCommentdefaultValue() {
    if (ratingIndex !== null && selectedRestaurant.ratings[ratingIndex]) {
      return selectedRestaurant.ratings[ratingIndex].comment || "";
    }
    return "";
  }

  return (
    <div>
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <button onClick={handleAddRestaurant}>Add Restaurant</button>
      </div>
      <Modal isOpen={isModalOpen} className="modal">
        <form onSubmit={handleSubmit} className="form">
          <label>
            Restaurant Name
            <input
              name="name"
              placeholder="Restaurant Name"
              onChange={handleInputChange}
              className="input"
            />
          </label>
          <div className="input-container">
            <label>
              Cheapest Menu
              <input
                name="cheapestMenuName"
                placeholder="Cheapest Menu"
                onChange={handleInputChange}
                className="input"
              />
            </label>
            <label>
              Price
              <input
                name="cheapestMenuPrice"
                placeholder="Price"
                onChange={handleInputChange}
                className="input"
              />
            </label>
          </div>
          <div className="input-container">
            <label>
              Expensive Wine
              <input
                name="expensiveWineName"
                placeholder="Expensive Wine"
                onChange={handleInputChange}
                className="input"
              />
            </label>
            <label>
              Price
              <input
                name="expensiveWinePrice"
                placeholder="Price"
                onChange={handleInputChange}
                className="input"
              />
            </label>
          </div>
          <label>
            Description
            <input
              name="description"
              placeholder="Description"
              onChange={handleInputChange}
              className="input"
            />
          </label>
          <label>
            Address
            <input
              name="address"
              placeholder="Address"
              onChange={handleInputChange}
              className="input"
            />
          </label>
          <div {...getRootProps({ className: "dropzone" })}>
            <p>Drag and Drop</p>
            <input {...getInputProps()} />
            {newRestaurant.image && (
              <div className="image-container">
                <img src={newRestaurant.image} alt="" className="preview" />
                <button
                  className="delete-icon"
                  onClick={() =>
                    setNewRestaurant({
                      ...newRestaurant,
                      image: null,
                    })
                  }
                >
                  X
                </button>
              </div>
            )}
            <button type="button" onClick={open}>
              Browse
            </button>
          </div>
          <button type="submit">Submit</button>
        </form>
      </Modal>
      <Modal isOpen={isPopupOpen} className="modal">
        {isPopupOpen && (
          <div className="modal-content flex-column">
            <div className="content-container">
              <div>
                <h2>{selectedRestaurant.name}</h2>
                <p>
                  <a
                    href={`https://www.google.com/maps/search/?api=1&query=${encodeURIComponent(
                      selectedRestaurant.address
                    )}`}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {selectedRestaurant.address}
                  </a>
                </p>
                <p>{selectedRestaurant.description}</p>
                <p>
                  Cheapest Menu: {selectedRestaurant.cheapestMenu.name}{" "}
                  {selectedRestaurant.cheapestMenu.price} .-
                </p>
                <p>
                  Most Expensive Wine: {selectedRestaurant.expensiveWine.name}{" "}
                  {selectedRestaurant.expensiveWine.price}.-
                </p>
              </div>
              {selectedRestaurant.image && (
                <img
                  src={selectedRestaurant.image}
                  alt={selectedRestaurant.name}
                />
              )}
              <div className="average-rating">
                {selectedRestaurant.ratings && (
                  <div>
                    <p>Average Rating:</p>
                    <div className="stars">
                      {Array(Math.floor(getAverageRating())).fill("⭐")}
                      {getAverageRating() % 1 !== 0 && "⭐️"}
                    </div>
                  </div>
                )}
              </div>
            </div>
            <div className="ratings-container scrollable">
              {selectedRestaurant.ratings &&
                selectedRestaurant.ratings.map((rating, index) => (
                  <div
                    key={index}
                    className="rating"
                    onClick={() => handleRatingClick(index)}
                  >
                    <div className="stars">
                      {Array(Math.floor(rating.rating)).fill("⭐")}
                      {rating.rating % 1 !== 0 && "⭐️"}
                    </div>
                    <p>{rating.comment}</p>
                    {ratingIndex === index && (
                      <div>
                        <StarRatings
                          className="star-ratings"
                          rating={newRating} // Use newRating instead of rating
                          starRatedColor="yellow"
                          starHoverColor="yellow"
                          changeRating={(newRating) => setNewRating(newRating)} // Update newRating state
                          numberOfStars={5}
                          name="rating"
                          starDimension="25px" // Adjust the size as needed
                          starSpacing="2px"
                        />
                        <br />
                        <textarea
                          className="comment-textarea-new"
                          defaultValue={setCommentdefaultValue()} // Use existing comment as default value
                          onChange={(e) => setNewComment(e.target.value)} // Update newComment state
                        />
                        <br />
                        <button
                          type="button"
                          onClick={(e) => handleRatingSubmit(e, index)}
                        >
                          Submit
                        </button>
                        <button
                          type="button"
                          onClick={() => handleDeleteComment(index)}
                        >
                          Delete
                        </button>
                      </div>
                    )}
                  </div>
                ))}
            </div>
            <div className="button-container">
              <div className="button-container">
                <button onClick={() => setIsPopupOpen(false)}>Close</button>
                {!isRatingOpen && (
                  <button onClick={() => setIsRatingOpen(true)}>
                    Add Rating
                  </button>
                )}
              </div>
            </div>
            {isRatingOpen && (
              <form onSubmit={handleRatingSubmit}>
                <div className="rating-container">
                  <label className="rating-label">Rating:</label>
                  <StarRatings
                    className="star-ratings"
                    rating={rating}
                    starRatedColor="yellow"
                    starHoverColor="yellow"
                    changeRating={(newRating) => setRating(newRating)}
                    numberOfStars={5}
                    name="rating"
                    starDimension="25px"
                    starSpacing="2px"
                  />
                </div>
                <div className="comment-container">
                  {" "}
                  <label>
                    Comment:
                    <textarea
                      className="comment-textarea"
                      value={comment}
                      onChange={(e) => setComment(e.target.value)}
                    />
                  </label>
                </div>

                <button onClick={onSubmitRating}>Submit Rating</button>
              </form>
            )}
          </div>
        )}
      </Modal>
      <div>
        <h1>Restaurants</h1>
        <div className="container">
          <div className="navbar">
            <input
              type="text"
              placeholder="Search..."
              value={searchTerm}
              onChange={(e) => setSearchTerm(e.target.value)}
            />
            <label>
              top 5 Restaurants
              <input
                type="checkbox"
                checked={showTopFive}
                onChange={() => setShowTopFive(!showTopFive)}
              />
            </label>
            <label>
              Menu Price:
              <input
                type="number"
                placeholder="0"
                value={menuPrice}
                onChange={(e) => setMenuPrice(e.target.value)}
                min="0"
                style={{ width: "60px" }}
              />
              <input
                type="checkbox"
                checked={isMenuPriceSearchActive}
                onChange={(e) => setIsMenuPriceSearchActive(e.target.checked)}
              />
            </label>
            <label>
              Wine Price:
              <input
                type="number"
                placeholder="0"
                value={winePrice}
                onChange={(e) => setWinePrice(e.target.value)}
                min="0"
                style={{ width: "60px" }}
              />
              <input
                type="checkbox"
                checked={isWinePriceSearchActive}
                onChange={(e) => setIsWinePriceSearchActive(e.target.checked)}
              />
            </label>
          </div>
          <div className="restaurants">
            {restaurantsToDisplay
              .filter((restaurant) =>
                restaurant.name.toLowerCase().includes(searchTerm.toLowerCase())
              )
              .map((restaurant) => {
                const totalStars = restaurant.ratings.reduce(
                  (total, rating) => total + rating.rating,
                  0
                );
                const averageStars =
                  restaurant.ratings.length > 0
                    ? totalStars / restaurant.ratings.length
                    : 0;

                return (
                  <div
                    key={restaurant.id}
                    className="restaurant"
                    onClick={() => handleRestaurantClick(restaurant)}
                  >
                    <div className="details">
                      <div className="top">
                        <h2>{restaurant.name}</h2>
                        <p className="address">{restaurant.address}</p>
                      </div>
                      <p className="description">{restaurant.description}</p>
                      <p>
                        {Array(Math.floor(averageStars)).fill("⭐")}
                        {averageStars % 1 !== 0 && "⭐️"}
                      </p>
                    </div>
                    {restaurant.image && (
                      <img src={restaurant.image} alt={restaurant.name} />
                    )}
                  </div>
                );
              })}
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
