// models/Restaurant.js
const mongoose = require("mongoose");

const RestaurantSchema = new mongoose.Schema({
  name: String,
  address: String,
  description: String,
  image: String,
  cheapestMenu: {
    name: String,
    price: Number,
  },
  expensiveWine: {
    name: String,
    price: Number,
  },
  ratings: {
    type: [
      {
        rating: Number,
        comment: String,
        time: { type: Date, default: Date.now },
      },
    ],
    default: [],
  },
});

module.exports = mongoose.model("restaurants", RestaurantSchema);
