// controllers/restaurantController.js
const Restaurant = require("./model");

exports.getAll = async (req, res) => {
  const restaurants = await Restaurant.find();
  res.json(restaurants);
};

exports.create = async (req, res) => {
  const restaurant = new Restaurant(req.body);
  await restaurant.save();
  res.json(restaurant);
};

exports.createRating = async (req, res) => {
  try {
    const { id, rating, comment } = req.body; // Include the comment in the request body
    const restaurant = await Restaurant.findById(id);
    if (!restaurant) {
      return res.status(404).send("Restaurant not found");
    }
    const currentTime = new Date();
    restaurant.ratings.push({ rating, comment, time: currentTime }); // Include the current time in the ratings
    await restaurant.save();
    res.status(200).send("Rating submitted");
  } catch (error) {
    console.error("Failed to submit rating", error);
    res.status(500).send("Failed to submit rating");
  }
};

exports.updateRating = async (req, res) => {
  const { id, ratings } = req.body;

  try {
    const restaurant = await Restaurant.findById(id);

    if (!restaurant) {
      return res.status(404).json({ message: "Restaurant not found" });
    }

    // Update the time for each rating
    const updatedRatings = ratings.map((rating) => ({
      ...rating,
      time: new Date(),
    }));

    restaurant.ratings = updatedRatings;

    await restaurant.save();

    res.json({ message: "Rating updated successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Failed to update rating" });
  }
};

exports.deleteRating = async (req, res) => {
  const { restaurantId, ratingIndex } = req.body;

  try {
    const restaurant = await Restaurant.findById(restaurantId);
    if (!restaurant) {
      return res.status(404).send("Restaurant not found");
    }

    restaurant.ratings.splice(ratingIndex, 1);
    await restaurant.save();

    res.sendStatus(200);
  } catch (error) {
    console.error("Failed to delete rating:", error);
    res.status(500).send("Failed to delete rating");
  }
};
