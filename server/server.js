// server.js
const express = require("express");
const mongoose = require("mongoose");
const RestaurantController = require("./controllers/controller");
const cors = require("cors");
const bodyParser = require("body-parser");

const app = express();
app.use(cors());
app.disable("etag");
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.use(bodyParser.json({ limit: "10mb" }));
app.use(bodyParser.urlencoded({ limit: "10mb", extended: true }));

mongoose.connect(
  "mongodb+srv://SinanM:TBrVZIEEv84WhC6Q@m323.elrtvqg.mongodb.net/M323"
);

app.get("/restaurants", RestaurantController.getAll);
app.post("/restaurants", RestaurantController.create);
app.post("/api/ratings", RestaurantController.createRating);
app.put("/api/ratings", RestaurantController.updateRating);
app.delete("/api/ratings", RestaurantController.deleteRating);

app.listen(4000, () => console.log("Server is running on port 4000"));
